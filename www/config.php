<?php
// BASE PATH
$shmotka_http_server = 'http://shmotka.com.ua/';
$shmotka_base_dir = '/home/shmotk06/shmotka.com.ua/www/';

// HTTP
define('HTTP_SERVER', $shmotka_http_server);

// HTTPS
define('HTTPS_SERVER', $shmotka_http_server);

// DIR
define('DIR_APPLICATION', $shmotka_base_dir.'catalog/');
define('DIR_SYSTEM', $shmotka_base_dir.'system/');
define('DIR_LANGUAGE', $shmotka_base_dir.'catalog/language/');
define('DIR_TEMPLATE', $shmotka_base_dir.'catalog/view/theme/');
define('DIR_CONFIG', $shmotka_base_dir.'system/config/');
define('DIR_IMAGE', $shmotka_base_dir.'image/');
define('DIR_CACHE', $shmotka_base_dir.'system/cache/');
define('DIR_DOWNLOAD', $shmotka_base_dir.'system/download/');
define('DIR_UPLOAD', $shmotka_base_dir.'system/upload/');
define('DIR_MODIFICATION', $shmotka_base_dir.'system/modification/');
define('DIR_LOGS', $shmotka_base_dir.'system/logs/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'shmotk06.mysql.ukraine.com.ua');
define('DB_USERNAME', 'shmotk06_clone');
define('DB_PASSWORD', 'mX8bV7k9');
define('DB_DATABASE', 'shmotk06_clone');
define('DB_PREFIX', 'tr_');
