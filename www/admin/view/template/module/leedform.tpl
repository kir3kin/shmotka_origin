<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-leedform" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
		<style>
			.st_div { margin-top: 50px; }
			.cb_div { margin-top: 10px; }

			@media (max-width: 767px) {
				.cb_div { margin-top: 0; }
			}
		</style>

        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-leedform" class="form-horizontal">
	        <div class="form-group">
	            <label class="col-sm-2 control-label">Заголовочный текст</label>

	            <div class="col-sm-10">
					<input type="text" class="form-control" name="leedform_header" value="<?php echo $leedform_header; ?>" placeholder="Заголовочный текст" />
	            </div>
	        </div>

	        <div class="form-group">
	            <label class="col-sm-2 control-label">Текст описания</label>

	            <div class="col-sm-10">
					<input type="text" class="form-control" name="leedform_description" value="<?php echo $leedform_description; ?>" placeholder="Текст описания" />
	            </div>
	        </div>

	        <div class="form-group">
				<label class="col-sm-2 control-label">Загрузка изображения</label>

				<div class="col-sm-10 cb_div">
					<input type="checkbox" name="leedform_upload" class="form-control"<?php if ($leedform_upload == '1') echo ' checked'; ?> />
				</div>
	        </div>

	        <div class="form-group">
	            <label class="col-sm-2 control-label">Текст загрузки</label>

	            <div class="col-sm-10">
					<input type="text" class="form-control" name="leedform_upload_text" value="<?php echo $leedform_upload_text; ?>" placeholder="Текст загрузки" />
	            </div>
	        </div>

	        <div class="form-group">
				<label class="col-sm-2 control-label">Включить для ПК</label>

				<div class="col-sm-10 cb_div">
					<input type="checkbox" name="leedform_pc" class="form-control"<?php if ($leedform_pc == '1') echo ' checked'; ?> />
				</div>
	        </div>

	        <div class="form-group">
				<label class="col-sm-2 control-label">Включить для мобильных</label>

				<div class="col-sm-10 cb_div">
					<input type="checkbox" name="leedform_mob" class="form-control"<?php if ($leedform_mob == '1') echo ' checked'; ?> />
				</div>
	        </div>

	        <div class="form-group st_div">
	            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
	            <div class="col-sm-10">
	              <select name="leedform_status" id="input-status" class="form-control">
	                <?php if ($leedform_status) { ?>
	                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
	                <option value="0"><?php echo $text_disabled; ?></option>
	                <?php } else { ?>
	                <option value="1"><?php echo $text_enabled; ?></option>
	                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
	                <?php } ?>
	              </select>
	            </div>
	        </div>
        </form>
      </div>
	</div>
  </div>
</div>
<?php echo $footer; ?>
