<script type="text/javascript">
	function addUploadHandler(btn, handler) {
		function uploadHandler() {
			$('#image_upload_form').remove();
			$('body').prepend('<form enctype="multipart/form-data" id="image_upload_form" style="display: none;"><input type="file" name="file" value="" /></form>');
			$('#image_upload_form input[name=\'file\']').trigger('click');

			timer = setInterval(function() {
				if ($('#image_upload_form input[name=\'file\']').val() != '') {
					clearInterval(timer);

					$.ajax({
						url: 'index.php?route=tool/imageupload/upload&token=' + getURLVar('token'),
						type: 'post',
						dataType: 'json',
						data: new FormData($('#image_upload_form')[0]),
						cache: false,
						contentType: false,
						processData: false,

						beforeSend: function() {
							btn.style.display = 'none';
							$('<i id="upload_button_replacer" class="fa fa-circle-o-notch fa-spin"></i>').insertAfter(btn);
						},

						complete: function() {
							btn.style.display = '';
							$('#upload_button_replacer').remove();
						},

						success: function(json) {
							if (handler) handler(json); else console.log(json);
						},

						error: function(xhr, opt, err) {
							alert(err + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
			}, 500);
		}

		$(btn).on('click', uploadHandler);
	}
</script>
