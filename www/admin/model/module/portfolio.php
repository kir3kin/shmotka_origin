<?php
class ModelModulePortfolio extends Model {
	public function get() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "portfolio_image ORDER BY portfolio_image_id, sort_order");
		return $query->rows;
	}

	public function add($path, $sort_order) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "portfolio_image (image, sort_order) VALUES ('" . $path . "', '" . $sort_order . "')");
		return $this->db->getLastId();
	}

	public function update($id, $path, $sort_order) {
		$this->db->query("UPDATE " . DB_PREFIX . "portfolio_image SET image='". $path . "', sort_order='" . $sort_order . "' WHERE portfolio_image_id=" . $id);
	}

	public function delete($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "portfolio_image WHERE portfolio_image_id=" . $id);
	}
}
