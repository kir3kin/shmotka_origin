<?php
class ControllerModuleRetargeting extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('module/retargeting');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('setting/setting');

        // Start If: Validates and check if data is coming by save (POST) method
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('retargeting', $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }

        // Assign the language data for parsing it to view
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_edit']    = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_content_top'] = $this->language->get('text_content_top');
        $data['text_content_bottom'] = $this->language->get('text_content_bottom');
        $data['text_column_left'] = $this->language->get('text_column_left');
        $data['text_column_right'] = $this->language->get('text_column_right');

        $data['entry_yandex'] = $this->language->get('entry_yandex');
        $data['entry_vk'] = $this->language->get('entry_vk');
        $data['entry_vk_success'] = $this->language->get('entry_vk_success');

        $data['entry_layout'] = $this->language->get('entry_layout');
        $data['entry_position'] = $this->language->get('entry_position');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_add_module'] = $this->language->get('button_add_module');
        $data['button_remove'] = $this->language->get('button_remove');

        // This Block returns the warning if any
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        // This Block returns the error code if any
        if (isset($this->error['code'])) {
            $data['error_code'] = $this->error['code'];
        } else {
            $data['error_code'] = '';
        }

        // Making of Breadcrumbs to be displayed on site
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/retargeting', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['action'] = $this->url->link('module/retargeting', 'token=' . $this->session->data['token'], 'SSL');
        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->post['retargeting_yandex'])) {
            $data['retargeting_yandex'] = $this->request->post['retargeting_yandex'];
        } else {
            $data['retargeting_yandex'] = $this->config->get('retargeting_yandex');
        }

        if (isset($this->request->post['retargeting_vk'])) {
            $data['retargeting_vk'] = $this->request->post['retargeting_vk'];
        } else {
            $data['retargeting_vk'] = $this->config->get('retargeting_vk');
        }

        if (isset($this->request->post['retargeting_vk_success'])) {
            $data['retargeting_vk_success'] = $this->request->post['retargeting_vk_success'];
        } else {
            $data['retargeting_vk_success'] = $this->config->get('retargeting_vk_success');
        }

        // This block parses the status (enabled / disabled)
        if (isset($this->request->post['retargeting_status'])) {
            $data['retargeting_status'] = $this->request->post['retargeting_status'];
        } else {
            $data['retargeting_status'] = $this->config->get('retargeting_status');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('module/retargeting.tpl', $data));
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'module/retargeting')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) return true; else return false;
    }
}
