<?php
// Heading
$_['heading_title']       = 'Ретаргетинг';

// Text
$_['text_module']         = 'Модули';
$_['text_edit']           = 'Настройки модуля';
$_['text_success']        = 'Настройки успешно изменены!';
$_['text_enabled']        = 'Включено';
$_['text_disabled']       = 'Отключено';

$_['entry_yandex']        = 'yandex';
$_['entry_vk']            = 'vk';
$_['entry_vk_success']    = 'vk success';
$_['entry_status']        = 'Статус:';

// Error
$_['error_permission']    = 'Внимание: Недостаточно полномочий для изменения модуля!';
$_['error_code']          = 'Обязательный параметр';
