<?php
class ControllerModuleAtimer extends Controller {
    public function index() {
        $this->load->language('module/atimer');

        $data['heading_title'] = $this->language->get('heading_title');
		$data['atimer_label_text'] = $this->config->get('atimer_label_text');

        $data['atimer_year'] = $this->config->get('atimer_year');
		$data['atimer_mon'] = $this->config->get('atimer_mon');
		$data['atimer_day'] = $this->config->get('atimer_day');
		$data['atimer_hour'] = $this->config->get('atimer_hour');
		$data['atimer_min'] = $this->config->get('atimer_min');

		$data['atimer_period_day'] = $this->config->get('atimer_period_day');
		$data['atimer_period_hour'] = $this->config->get('atimer_period_hour');
		$data['atimer_period_min'] = $this->config->get('atimer_period_min');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/atimer.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/atimer.tpl', $data);
        } else {
            return $this->load->view('default/template/module/atimer.tpl', $data);
        }
    }
}
