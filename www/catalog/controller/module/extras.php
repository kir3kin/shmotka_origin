<?php  
class ControllerModuleExtras extends Controller {
	public function index() {
		$this->language->load('module/extras');
		
    	$data['heading_title'] = $this->language->get('heading_title');
    	
		$data['text_brands'] 		= $this->language->get('text_brands');
    	$data['text_gift']		= $this->language->get('text_gift');
		$data['text_affiliates']	= $this->language->get('text_affiliates');
		$data['text_specials'] 	= $this->language->get('text_specials');
		
		$data['brands']		= $this->url->link('product/manufacturer');
    	$data['gift'] 		= $this->url->link('checkout/voucher');
		$data['affiliates']	= $this->url->link('affiliate/account');
		$data['specials'] 	= $this->url->link('product/special');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/extras.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/extras.tpl', $data);
		} else {
			return $this->load->view('default/template/module/extras.tpl', $data);
		}
	}
}
?>