<h3><?php echo $heading_title; ?></h3>
<?php require(DIR_TEMPLATE.'default/template/product/buy_now.tpl'); ?>

<div class="row">
  <?php foreach ($products as $product) { ?>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="height: 320px">
    <div class="product-thumb transition">
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
      <div class="caption">
        <h5><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h5>
        <?php if ($product['rating']) { ?>
        <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>
        <?php if ($product['price']) { ?>
        <p class="price" style="font-size: 12px;">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>

          <div>
			<a href="#modBuyNow" data-toggle="modal" onclick="product_id = '<?php echo $product['product_id']; ?>';" class="btn btn-primary" style="margin-top: 1px;">Купить сейчас</a>
	        <a href="" title="Добавить в корзину" onclick="cart.add('<?php echo $product['product_id']; ?>'); return false;"><img src="image/buy.png" /></a>
	      </div>
        </p>
        <?php } ?>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
