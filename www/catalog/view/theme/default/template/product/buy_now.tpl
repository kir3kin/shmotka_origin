<div class="modal fade" id="modBuyNow" tabindex="-1" role="dialog" aria-labelledby="buyModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="text-align: left;">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h3 class="modal-title" id="buyModalLabel">Купить сейчас</h3>
			</div>

			<style>
				@media (max-width: 478px) {
					.mob_hide { display: none; }
				}
			</style>

			<div class="modal-body" style="text-align: left; color: #444;">
				<label>Имя, Фамилия</label>
				<input type="text" name="firstname" class="form-control" required>

				<label>Телефон</label>
				<input type="text" maxlength="20" name="telephone" class="form-control" required>

				<label class="mob_hide">Адрес доставки (город, номер отделения Новой Почты)</label>
				<input type="text" name="address" class="form-control mob_hide">

				<label class="mob_hide">Комментарии к заказу</label>
				<textarea rows="2" type="text" name="comment" class="form-control mob_hide"></textarea>

				<div class="modal-footer" style="text-align: right;">
					<div id="button_order" class="btn btn-primary">Отправить</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var product_id = '',
		firstname = $("input[name='firstname']")[0],
		telephone = $("input[name='telephone']").keypress(testPhoneInput)[0],
		address = $("input[name='address']")[0],
		comment = $("textarea[name='comment']")[0];

	$('#button_order').click(function() {
		if (product_id == '') return;

		if (firstname.value == '' || firstname.value.length > 32) { alert('Имя должно быть от 1 до 32 символов!'); return; }
		if (telephone.value == '') { alert('Номер телефона не соответствует заданному шаблону!'); return; }
		if (address.value.length < 3) address.value = '---';

		$.ajax({
            url: 'index.php?route=checkout/buy/buy_now',
            type: 'post',
            dataType: 'json',

            data: {
				product_id: product_id,
				firstname: firstname.value,
				telephone: telephone.value,
				address: address.value,
				comment: comment.value
			},

            success: function(data) {
				if (data.success == '1') location = 'index.php?route=checkout/success';
				else console.log(data);
			},

			error: function(xhr, opt, err) { alert(err); }
        });

        product_id = '';
	});
</script>
